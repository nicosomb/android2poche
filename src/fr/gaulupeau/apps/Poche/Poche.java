/**
 * Android to Poche
 * A simple app to make the full save bookmark to Poche
 * web page available via the Share menu on Android tablets
 * @author GAULUPEAU Jonathan
 * August 2013
 */

package fr.gaulupeau.apps.Poche;

import java.io.UnsupportedEncodingException;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Main activity class
 */
@TargetApi(Build.VERSION_CODES.FROYO) public class Poche extends Activity {
	TextView authorSite; // the author and site line on the information page
	Button btnDone; // done/close button
	EditText editPocheUrl;
	public static final String PREFS_NAME = "MyPrefsFile";
    /** Called when the activity is first created. 
     * Will act differently depending on whether sharing or
     * displaying information page. */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
        // Get information about the call to start this activity
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        String action = intent.getAction();
        
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String pocheUrl = settings.getString("pocheUrl", "");
        
        // Find out if Sharing or if app has been launched from icon
        if (action.equals(Intent.ACTION_SEND)) {
        	// ACTION_SEND is called when sharing, get the title and URL from 
        	// the call
        	String pageUrl = extras.getString("android.intent.extra.TEXT");
			String pageTitle = extras.getString("android.intent.extra.SUBJECT");
			// Some apps do not provide the page title, so in this case use the URL again
            if (pageTitle == null) {
            	pageTitle = pageUrl;
            }
            // Start to build the Delicious URL
			Uri.Builder deliciousSaveUrl = Uri.parse(pocheUrl).buildUpon();
			// Add the parameters from the call
			deliciousSaveUrl.appendQueryParameter("action", "add");
			byte[] data = null;
			try {
				data = pageUrl.getBytes("UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String base64 = Base64.encodeToString(data, Base64.DEFAULT);
			deliciousSaveUrl.appendQueryParameter("url", base64);
			//System.out.println("base64 : " + base64);
			//System.out.println("pageurl : " + pageUrl);
			
			// Load the constructed URL in the browser
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(deliciousSaveUrl.build());
			// If user has more then one browser installed give them a chance to
			// select which one they want to use 
			startActivity(Intent.createChooser(i, getString(R.string.which_browser)));
			// That is all this app needs to do, so call finish()
			this.finish();
        }
        else {
        	// app has been launched from menu - show information window
        	setContentView(R.layout.main);
            // handle done/close button
        	editPocheUrl = (EditText)findViewById(R.id.pocheUrl);
        	editPocheUrl.setText(pocheUrl);
        	
            btnDone = (Button)findViewById(R.id.btnDone);
            btnDone.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					// close the app
					System.out.println(editPocheUrl.getText().toString());
					SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		        	SharedPreferences.Editor editor = settings.edit();
		        	editor.putString("pocheUrl", editPocheUrl.getText().toString());
					editor.commit();
					Poche.this.finish();
				}
            });
        }
    }
}
